# processing snake game

## controls
left -> q

right -> d

up -> z

down -> s 

## description
snake game written in Processing (java).

## howto
* put directory SnakeGame into your sketchbook directory
* open SnakeGame.pde with processing

## licence 
MIT

## version 
0.1

## notes
No input delay. So don't press controls too quickly !

## references

https://processing.org/

https://processing.org/reference/random_.html

https://processing.org/reference/frameCount.html
