Snake snake;
Food food;


int frameInvul = 60;
int delayInvul = 30;


void setup() {
  size(1000, 500);
  snake = new Snake(new Segment(100,100));
  food = new Food(width/2, height/2);
}

void draw() {
  background(0);
  
  if(frameCount%3==0){
   snake.updatePosition();
  }
  snake.display();

  food.display();


  //check if snake eats food
  if(snake.body.get(0).x==food.x && snake.body.get(0).y==food.y){
    snake.addSegment();
    frameInvul = frameCount;
    food = new Food(int(random(0, width))/10*10, int(random(0, height))/10*10);
  }

  //check if snake eats itself
  if(snake.body.size()>1){
    for(int i=1;i<snake.body.size();i++){
      if(snake.body.get(0).x == snake.body.get(i).x && snake.body.get(0).y == snake.body.get(i).y && frameCount > frameInvul + delayInvul) exit();
    }
  }

  //check out of bounds
  if(snake.body.get(0).x<0 || snake.body.get(0).x>width || snake.body.get(0).y<0 || snake.body.get(0).y>height) exit();
}

void keyPressed(){
  switch(key){
    case 'd' :
      if(snake.facing != 'L'){
	snake.facing = 'R';
      }
      break;
    case 'q' :
      if (snake.facing != 'R') {
	snake.facing = 'L';
      }
      break;
    case 's':
      if(snake.facing != 'U') {
	snake.facing = 'D';
      }
      break;
    case 'z':
      if (snake.facing !='D'){
	snake.facing = 'U';
      }
      break;
    default: break;
  }
}
