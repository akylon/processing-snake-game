class Snake {
  char facing = 'R';
  int mvSpeed = 10;
  ArrayList<Segment> body;
  
  Snake (Segment b){
    this.body = new ArrayList<>();
    this.body.add(b);
  }
  
  void addSegment(){//add new Segment at the end of body
    this.body.add(new Segment(this.body.get(this.body.size()-1).x, this.body.get(this.body.size()-1).y ));
  }
  
  void display(){
    for(int i=0;i<this.body.size();i++){
      this.body.get(i).display();
    }
  }
  
  void updatePosition(){
    int xp=0,yp=0,xp2=0,yp2=0;
    
    //save starting position of head
    xp = body.get(0).x;
    yp = body.get(0).y;

    switch(this.facing){
      case 'R': 
        body.get(0).x+=mvSpeed;
        break;
      case 'L': 
        body.get(0).x-=mvSpeed;
        break;
      case 'U':
        body.get(0).y-=mvSpeed;
        break;
      case 'D': 
        body.get(0).y+=mvSpeed;
        break;
    }


    for (int i=1;i<body.size();i++){
      if(i%2!=0){
        //store its old values
        xp2 = body.get(i).x;
        yp2 = body.get(i).y;
        
        //update to values of old segment
        body.get(i).x = xp;
        body.get(i).y = yp;
      }
      else {
        //store old values
        xp = body.get(i).x;
        yp = body.get(i).y;
        
        //update to values of previous segment
        body.get(i).x = xp2;
        body.get(i).y = yp2; 
      }
    }
  }
}
