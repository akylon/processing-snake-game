public class Segment {
  int x,y;
  int len = 10;
  
  public Segment(int x, int y){
    this.x=x;
    this.y = y;
  }
  
  void display(){
    fill(255,0,0);
    rect(this.x,this.y,this.len,this.len);
  }
}
