public class Food {
  int x, y;
  int size = 10;
  public Food(int x, int y){
    this.x = x;
    this.y = y;
  }

  public void display(){
    fill(120);
    rect(this.x, this.y, this.size, this.size);
  }
}
